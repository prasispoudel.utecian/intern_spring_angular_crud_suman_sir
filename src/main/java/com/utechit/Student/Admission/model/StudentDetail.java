package com.utechit.Student.Admission.model;
// import java.persistence.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "students")
public class StudentDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "id",unique = true)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "phone")
    private String phone;
    @Column(name = "address")
    private String address;
    @Column(name ="gender")
    private String gender;
    // @Column(name="DOB")
    // private Date DOB;
    @Column(name = "fathername")
    private String fathername;
    @Column(name ="mothername")
    private String mothername;
    // @ManyToOne
    public StudentDetail(){

    }
    public StudentDetail(int id, String name, String phone, String address, String gender, String fathername, String mothername){
        this.id=id;
        this.name=name;
        this.phone=phone;
        this.address=address;
        this.gender=gender;
        this.fathername=fathername;
        this.mothername=mothername;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
/*
    public Date getDOB() {
        return DOB;
    }

    public void setDOB(Date DOB) {
        this.DOB = DOB;
    }

 */

    public String getfathername() {
        return fathername;
    }

    public void setfathername(String fathername) {
        this.fathername = fathername;
    }

    public String getmothername() {
        return mothername;
    }

    public void setmothername(String mothername) {
        this.mothername = mothername;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", gender='" + gender + '\'' +
                ", fathername='" + fathername + '\'' +
                ", mothername='" + mothername + '\'' +
                '}';
    }
}
