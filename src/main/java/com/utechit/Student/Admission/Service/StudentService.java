package com.utechit.Student.Admission.Service;
import com.utechit.Student.Admission.Repository.StudentRepository;
import com.utechit.Student.Admission.model.StudentDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private StudentRepository studentRepository;
    @Autowired
    public StudentService(StudentRepository studentRepository){
        this.studentRepository=studentRepository;
    }
    public List<StudentDetail> getStudentList(){
        return (List<StudentDetail>) this.studentRepository.findAll();
    }
    public void addStudent(StudentDetail studentDetail){
        this.studentRepository.save(studentDetail);
    }
    public StudentDetail findById(Integer id){
        return this.studentRepository.findById(id).get();
    }
    public void deleteById(Integer id){
        this.studentRepository.deleteById(id);
    }
    public List<StudentDetail> findByName(String Name){
        return this.studentRepository.findByName(Name);
    }
    public void updateStudent(int id, StudentDetail studentDetail){
      Optional<StudentDetail> studentDetail1 = this.studentRepository.findById(id);
      String name = studentDetail1.get().getName();
      String phone = studentDetail1.get().getPhone();
      String address = studentDetail1.get().getAddress();
      String gender = studentDetail1.get().getGender();
      String fathername = studentDetail1.get().getfathername();
      String mothername = studentDetail1.get().getmothername();
      name = studentDetail.getName();
      phone = studentDetail.getPhone();
      address = studentDetail.getAddress();
      gender = studentDetail.getGender();
      fathername = studentDetail.getfathername();
      mothername = studentDetail.getmothername();
      this.studentRepository.save(new StudentDetail(id,name,phone,address,gender,fathername,mothername));

    }

}
