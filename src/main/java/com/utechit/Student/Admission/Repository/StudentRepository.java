package com.utechit.Student.Admission.Repository;
import com.utechit.Student.Admission.model.StudentDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface StudentRepository extends CrudRepository<StudentDetail,  Integer> {
    List<StudentDetail> findByName(String Name);

}
