package com.utechit.Student.Admission.Controller;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
// Importing Student Service
import com.utechit.Student.Admission.Service.StudentService;
// Importing data Student(table)(data model for the table)
import com.utechit.Student.Admission.model.StudentDetail;
@RestController
@RequestMapping("api/student/")
// @CrossOrigin(origins="http://localhost:4200")
@CrossOrigin(origins = "*")
public class StudentAPIcontroller {
      private StudentService studentService;
      @Autowired
      public StudentAPIcontroller(StudentService studentService){
          this.studentService=studentService;
      }
      @GetMapping
      public List<StudentDetail> getStudents(){
          return this.studentService.getStudentList();
      }
      @PostMapping("addStudent")
      @ResponseBody
      public void addStudent(@RequestBody StudentDetail studentDetail){
         this.studentService.addStudent(studentDetail);
      }
      @DeleteMapping("/{id}")
      public HttpStatus deletestudent(@PathVariable("id") Integer id){
          this.studentService.deleteById(id);
          return HttpStatus.OK;
      }
      @GetMapping("/{id}")
      public List<StudentDetail> getdetail(@PathVariable("id") Integer id){
          return (List<StudentDetail>) this.studentService.findById(id);
      }

      @PutMapping("/{id}")
      @ResponseBody
      public HttpStatus update(@PathVariable("id") Integer id,@RequestBody StudentDetail studentDetail){
          this.studentService.updateStudent(id,studentDetail);
          return HttpStatus.OK;
      }
}
